package com.eyeo.ctu.engine.fb

import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.adblockplus.fb.Index
import com.eyeo.ctu.engine.fb.benchmark.R
import org.junit.Rule
import org.junit.Test
import java.nio.ByteBuffer

class FlatBuffersLoadBenchmark {

    @get:Rule
    val benchmarkRule = BenchmarkRule()

    @Test
    fun testLoad() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        benchmarkRule.measureRepeated {
            val inputStream = context.resources.openRawResource(R.raw.easylist_and_exceptionrules)
            val index = Index.getRootAsIndex(ByteBuffer.wrap(inputStream.readBytes()))
        }
    }
}