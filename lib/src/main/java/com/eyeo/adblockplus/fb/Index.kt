// automatically generated by the FlatBuffers compiler, do not modify

package com.eyeo.adblockplus.fb

import java.nio.*
import kotlin.math.sign
import com.google.flatbuffers.*

@Suppress("unused")
@ExperimentalUnsignedTypes
class Index : Table() {

    fun __init(_i: Int, _bb: ByteBuffer)  {
        __reset(_i, _bb)
    }
    fun __assign(_i: Int, _bb: ByteBuffer) : Index {
        __init(_i, _bb)
        return this
    }
    fun filtersByKeyword(j: Int) : com.eyeo.adblockplus.fb.FiltersByKeyword? = filtersByKeyword(com.eyeo.adblockplus.fb.FiltersByKeyword(), j)
    fun filtersByKeyword(obj: com.eyeo.adblockplus.fb.FiltersByKeyword, j: Int) : com.eyeo.adblockplus.fb.FiltersByKeyword? {
        val o = __offset(4)
        return if (o != 0) {
            obj.__assign(__indirect(__vector(o) + j * 4), bb)
        } else {
            null
        }
    }
    val filtersByKeywordLength : Int
        get() {
            val o = __offset(4); return if (o != 0) __vector_len(o) else 0
        }
    fun filtersByKeywordByKey(key: String) : com.eyeo.adblockplus.fb.FiltersByKeyword? {
        val o = __offset(4)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FiltersByKeyword.__lookup_by_key(null, __vector(o), key, bb)
        } else {
            null
        }
    }
    fun filtersByKeywordByKey(obj: com.eyeo.adblockplus.fb.FiltersByKeyword, key: String) : com.eyeo.adblockplus.fb.FiltersByKeyword? {
        val o = __offset(4)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FiltersByKeyword.__lookup_by_key(obj, __vector(o), key, bb)
        } else {
            null
        }
    }
    fun filterBySelector(j: Int) : com.eyeo.adblockplus.fb.FilterBySelector? = filterBySelector(com.eyeo.adblockplus.fb.FilterBySelector(), j)
    fun filterBySelector(obj: com.eyeo.adblockplus.fb.FilterBySelector, j: Int) : com.eyeo.adblockplus.fb.FilterBySelector? {
        val o = __offset(6)
        return if (o != 0) {
            obj.__assign(__indirect(__vector(o) + j * 4), bb)
        } else {
            null
        }
    }
    val filterBySelectorLength : Int
        get() {
            val o = __offset(6); return if (o != 0) __vector_len(o) else 0
        }
    fun filterBySelectorByKey(key: String) : com.eyeo.adblockplus.fb.FilterBySelector? {
        val o = __offset(6)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FilterBySelector.__lookup_by_key(null, __vector(o), key, bb)
        } else {
            null
        }
    }
    fun filterBySelectorByKey(obj: com.eyeo.adblockplus.fb.FilterBySelector, key: String) : com.eyeo.adblockplus.fb.FilterBySelector? {
        val o = __offset(6)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FilterBySelector.__lookup_by_key(obj, __vector(o), key, bb)
        } else {
            null
        }
    }
    fun filtersByDomain(j: Int) : com.eyeo.adblockplus.fb.FiltersByDomain? = filtersByDomain(com.eyeo.adblockplus.fb.FiltersByDomain(), j)
    fun filtersByDomain(obj: com.eyeo.adblockplus.fb.FiltersByDomain, j: Int) : com.eyeo.adblockplus.fb.FiltersByDomain? {
        val o = __offset(8)
        return if (o != 0) {
            obj.__assign(__indirect(__vector(o) + j * 4), bb)
        } else {
            null
        }
    }
    val filtersByDomainLength : Int
        get() {
            val o = __offset(8); return if (o != 0) __vector_len(o) else 0
        }
    fun filtersByDomainByKey(key: String) : com.eyeo.adblockplus.fb.FiltersByDomain? {
        val o = __offset(8)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FiltersByDomain.__lookup_by_key(null, __vector(o), key, bb)
        } else {
            null
        }
    }
    fun filtersByDomainByKey(obj: com.eyeo.adblockplus.fb.FiltersByDomain, key: String) : com.eyeo.adblockplus.fb.FiltersByDomain? {
        val o = __offset(8)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FiltersByDomain.__lookup_by_key(obj, __vector(o), key, bb)
        } else {
            null
        }
    }
    fun elemhideKnownFilters(j: Int) : String? {
        val o = __offset(10)
        return if (o != 0) {
            __string(__vector(o) + j * 4)
        } else {
            null
        }
    }
    val elemhideKnownFiltersLength : Int
        get() {
            val o = __offset(10); return if (o != 0) __vector_len(o) else 0
        }
    fun elemhideExceptionKnownExceptions(j: Int) : String? {
        val o = __offset(12)
        return if (o != 0) {
            __string(__vector(o) + j * 4)
        } else {
            null
        }
    }
    val elemhideExceptionKnownExceptionsLength : Int
        get() {
            val o = __offset(12); return if (o != 0) __vector_len(o) else 0
        }
    fun elemhideExceptionExceptions(j: Int) : com.eyeo.adblockplus.fb.FiltersBySelector? = elemhideExceptionExceptions(com.eyeo.adblockplus.fb.FiltersBySelector(), j)
    fun elemhideExceptionExceptions(obj: com.eyeo.adblockplus.fb.FiltersBySelector, j: Int) : com.eyeo.adblockplus.fb.FiltersBySelector? {
        val o = __offset(14)
        return if (o != 0) {
            obj.__assign(__indirect(__vector(o) + j * 4), bb)
        } else {
            null
        }
    }
    val elemhideExceptionExceptionsLength : Int
        get() {
            val o = __offset(14); return if (o != 0) __vector_len(o) else 0
        }
    fun elemhideExceptionExceptionsByKey(key: String) : com.eyeo.adblockplus.fb.FiltersBySelector? {
        val o = __offset(14)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FiltersBySelector.__lookup_by_key(null, __vector(o), key, bb)
        } else {
            null
        }
    }
    fun elemhideExceptionExceptionsByKey(obj: com.eyeo.adblockplus.fb.FiltersBySelector, key: String) : com.eyeo.adblockplus.fb.FiltersBySelector? {
        val o = __offset(14)
        return if (o != 0) {
            com.eyeo.adblockplus.fb.FiltersBySelector.__lookup_by_key(obj, __vector(o), key, bb)
        } else {
            null
        }
    }
    companion object {
        fun validateVersion() = Constants.FLATBUFFERS_1_12_0()
        fun getRootAsIndex(_bb: ByteBuffer): Index = getRootAsIndex(_bb, Index())
        fun getRootAsIndex(_bb: ByteBuffer, obj: Index): Index {
            _bb.order(ByteOrder.LITTLE_ENDIAN)
            return (obj.__assign(_bb.getInt(_bb.position()) + _bb.position(), _bb))
        }
        fun createIndex(builder: FlatBufferBuilder, filtersByKeywordOffset: Int, filterBySelectorOffset: Int, filtersByDomainOffset: Int, elemhideKnownFiltersOffset: Int, elemhideExceptionKnownExceptionsOffset: Int, elemhideExceptionExceptionsOffset: Int) : Int {
            builder.startTable(6)
            addElemhideExceptionExceptions(builder, elemhideExceptionExceptionsOffset)
            addElemhideExceptionKnownExceptions(builder, elemhideExceptionKnownExceptionsOffset)
            addElemhideKnownFilters(builder, elemhideKnownFiltersOffset)
            addFiltersByDomain(builder, filtersByDomainOffset)
            addFilterBySelector(builder, filterBySelectorOffset)
            addFiltersByKeyword(builder, filtersByKeywordOffset)
            return endIndex(builder)
        }
        fun startIndex(builder: FlatBufferBuilder) = builder.startTable(6)
        fun addFiltersByKeyword(builder: FlatBufferBuilder, filtersByKeyword: Int) = builder.addOffset(0, filtersByKeyword, 0)
        fun createFiltersByKeywordVector(builder: FlatBufferBuilder, data: IntArray) : Int {
            builder.startVector(4, data.size, 4)
            for (i in data.size - 1 downTo 0) {
                builder.addOffset(data[i])
            }
            return builder.endVector()
        }
        fun startFiltersByKeywordVector(builder: FlatBufferBuilder, numElems: Int) = builder.startVector(4, numElems, 4)
        fun addFilterBySelector(builder: FlatBufferBuilder, filterBySelector: Int) = builder.addOffset(1, filterBySelector, 0)
        fun createFilterBySelectorVector(builder: FlatBufferBuilder, data: IntArray) : Int {
            builder.startVector(4, data.size, 4)
            for (i in data.size - 1 downTo 0) {
                builder.addOffset(data[i])
            }
            return builder.endVector()
        }
        fun startFilterBySelectorVector(builder: FlatBufferBuilder, numElems: Int) = builder.startVector(4, numElems, 4)
        fun addFiltersByDomain(builder: FlatBufferBuilder, filtersByDomain: Int) = builder.addOffset(2, filtersByDomain, 0)
        fun createFiltersByDomainVector(builder: FlatBufferBuilder, data: IntArray) : Int {
            builder.startVector(4, data.size, 4)
            for (i in data.size - 1 downTo 0) {
                builder.addOffset(data[i])
            }
            return builder.endVector()
        }
        fun startFiltersByDomainVector(builder: FlatBufferBuilder, numElems: Int) = builder.startVector(4, numElems, 4)
        fun addElemhideKnownFilters(builder: FlatBufferBuilder, elemhideKnownFilters: Int) = builder.addOffset(3, elemhideKnownFilters, 0)
        fun createElemhideKnownFiltersVector(builder: FlatBufferBuilder, data: IntArray) : Int {
            builder.startVector(4, data.size, 4)
            for (i in data.size - 1 downTo 0) {
                builder.addOffset(data[i])
            }
            return builder.endVector()
        }
        fun startElemhideKnownFiltersVector(builder: FlatBufferBuilder, numElems: Int) = builder.startVector(4, numElems, 4)
        fun addElemhideExceptionKnownExceptions(builder: FlatBufferBuilder, elemhideExceptionKnownExceptions: Int) = builder.addOffset(4, elemhideExceptionKnownExceptions, 0)
        fun createElemhideExceptionKnownExceptionsVector(builder: FlatBufferBuilder, data: IntArray) : Int {
            builder.startVector(4, data.size, 4)
            for (i in data.size - 1 downTo 0) {
                builder.addOffset(data[i])
            }
            return builder.endVector()
        }
        fun startElemhideExceptionKnownExceptionsVector(builder: FlatBufferBuilder, numElems: Int) = builder.startVector(4, numElems, 4)
        fun addElemhideExceptionExceptions(builder: FlatBufferBuilder, elemhideExceptionExceptions: Int) = builder.addOffset(5, elemhideExceptionExceptions, 0)
        fun createElemhideExceptionExceptionsVector(builder: FlatBufferBuilder, data: IntArray) : Int {
            builder.startVector(4, data.size, 4)
            for (i in data.size - 1 downTo 0) {
                builder.addOffset(data[i])
            }
            return builder.endVector()
        }
        fun startElemhideExceptionExceptionsVector(builder: FlatBufferBuilder, numElems: Int) = builder.startVector(4, numElems, 4)
        fun endIndex(builder: FlatBufferBuilder) : Int {
            val o = builder.endTable()
            return o
        }
        fun finishIndexBuffer(builder: FlatBufferBuilder, offset: Int) = builder.finish(offset)
        fun finishSizePrefixedIndexBuffer(builder: FlatBufferBuilder, offset: Int) = builder.finishSizePrefixed(offset)
    }
}
