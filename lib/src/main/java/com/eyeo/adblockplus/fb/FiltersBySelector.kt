// automatically generated by the FlatBuffers compiler, do not modify

package com.eyeo.adblockplus.fb

import java.nio.*
import kotlin.math.sign
import com.google.flatbuffers.*

@Suppress("unused")
@ExperimentalUnsignedTypes
class FiltersBySelector : Table() {

    fun __init(_i: Int, _bb: ByteBuffer)  {
        __reset(_i, _bb)
    }
    fun __assign(_i: Int, _bb: ByteBuffer) : FiltersBySelector {
        __init(_i, _bb)
        return this
    }
    val selector : String?
        get() {
            val o = __offset(4)
            return if (o != 0) __string(o + bb_pos) else null
        }
    val selectorAsByteBuffer : ByteBuffer get() = __vector_as_bytebuffer(4, 1)
    fun selectorInByteBuffer(_bb: ByteBuffer) : ByteBuffer = __vector_in_bytebuffer(_bb, 4, 1)
    fun filters(j: Int) : com.eyeo.adblockplus.fb.ContentFilter? = filters(com.eyeo.adblockplus.fb.ContentFilter(), j)
    fun filters(obj: com.eyeo.adblockplus.fb.ContentFilter, j: Int) : com.eyeo.adblockplus.fb.ContentFilter? {
        val o = __offset(6)
        return if (o != 0) {
            obj.__assign(__indirect(__vector(o) + j * 4), bb)
        } else {
            null
        }
    }
    val filtersLength : Int
        get() {
            val o = __offset(6); return if (o != 0) __vector_len(o) else 0
        }
    override fun keysCompare(o1: Int, o2: Int, _bb: ByteBuffer) : Int {
         return compareStrings(__offset(4, o1, _bb), __offset(4, o2, _bb), _bb)
    }
    companion object {
        fun validateVersion() = Constants.FLATBUFFERS_1_12_0()
        fun getRootAsFiltersBySelector(_bb: ByteBuffer): FiltersBySelector = getRootAsFiltersBySelector(_bb, FiltersBySelector())
        fun getRootAsFiltersBySelector(_bb: ByteBuffer, obj: FiltersBySelector): FiltersBySelector {
            _bb.order(ByteOrder.LITTLE_ENDIAN)
            return (obj.__assign(_bb.getInt(_bb.position()) + _bb.position(), _bb))
        }
        fun createFiltersBySelector(builder: FlatBufferBuilder, selectorOffset: Int, filtersOffset: Int) : Int {
            builder.startTable(2)
            addFilters(builder, filtersOffset)
            addSelector(builder, selectorOffset)
            return endFiltersBySelector(builder)
        }
        fun startFiltersBySelector(builder: FlatBufferBuilder) = builder.startTable(2)
        fun addSelector(builder: FlatBufferBuilder, selector: Int) = builder.addOffset(0, selector, 0)
        fun addFilters(builder: FlatBufferBuilder, filters: Int) = builder.addOffset(1, filters, 0)
        fun createFiltersVector(builder: FlatBufferBuilder, data: IntArray) : Int {
            builder.startVector(4, data.size, 4)
            for (i in data.size - 1 downTo 0) {
                builder.addOffset(data[i])
            }
            return builder.endVector()
        }
        fun startFiltersVector(builder: FlatBufferBuilder, numElems: Int) = builder.startVector(4, numElems, 4)
        fun endFiltersBySelector(builder: FlatBufferBuilder) : Int {
            val o = builder.endTable()
                builder.required(o, 4)
                builder.required(o, 6)
            return o
        }
        fun __lookup_by_key(obj: FiltersBySelector?, vectorLocation: Int, key: String, bb: ByteBuffer) : FiltersBySelector? {
            val byteKey = key.toByteArray(java.nio.charset.StandardCharsets.UTF_8)
            var span = bb.getInt(vectorLocation - 4)
            var start = 0
            while (span != 0) {
                var middle = span / 2
                val tableOffset = __indirect(vectorLocation + 4 * (start + middle), bb)
                val comp = compareStrings(__offset(4, bb.capacity() - tableOffset, bb), byteKey, bb)
                when {
                    comp > 0 -> span = middle
                    comp < 0 -> {
                        middle++
                        start += middle
                        span -= middle
                    }
                    else -> {
                        return (obj ?: FiltersBySelector()).__assign(tableOffset, bb)
                    }
                }
            }
            return null
        }
    }
}
