package com.eyeo.ctu.fb

import android.os.Debug
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.adblockplus.fb.Index
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import java.nio.ByteBuffer
import kotlin.system.measureTimeMillis

@RunWith(AndroidJUnit4::class)
class FlatBuffersLoadTest {

    private fun measureConsumptionKb(): Int {
        System.gc()
        System.gc()
        System.gc()

        // let it collect the garbage
        Thread.sleep(3_000)

        val memInfoVar = Debug.MemoryInfo()
        Debug.getMemoryInfo(memInfoVar)
        val slices = arrayListOf(
            "summary.private-other",
            "summary.system",
            "summary.code",
            "summary.stack",
            "summary.graphics",
            "summary.java-heap", // seems to be the most/only meaningful value to measure
            "summary.native-heap"
        )
        var consumption = 0
        for (eachSlice in slices) {
            consumption += memInfoVar.getMemoryStat(eachSlice).toInt() // the values are in Kb
        }
        return consumption
    }

    private fun loadAndMeasureConsumption(resourceId: Int): Index {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        lateinit var index: Index

        val consumedBeforeKb = measureConsumptionKb()
        val loadTimeMs = measureTimeMillis {
            context.resources.openRawResource(resourceId).use {
                index = Index.getRootAsIndex(ByteBuffer.wrap(it.readBytes()))
            }
        }
        val consumedAfterKb = measureConsumptionKb()
        val consumedKb = consumedAfterKb - consumedBeforeKb
        println("Loaded within $loadTimeMs ms and consumed $consumedKb Kb")
        return index
    }

    @Test
    fun testMeasureEasylist() {
        loadAndMeasureConsumption(R.raw.easylist)
    }

    @Test
    fun testMeasureEasylistLatest() {
        loadAndMeasureConsumption(R.raw.easylist_latest)
    }

    @Test
    fun testMeasureEasyAndExceptionRules() {
        loadAndMeasureConsumption(R.raw.easylist_and_exceptionrules)
    }

    @Test
    fun testLoad() {
        val index = loadAndMeasureConsumption(R.raw.easylist_and_exceptionrules)

        assertEquals(12_443, index.filtersByDomainLength)
        assertEquals(25_975, index.elemhideKnownFiltersLength)
        assertEquals(21_314, index.filterBySelectorLength)
        assertEquals(1_663, index.elemhideExceptionKnownExceptionsLength)

        // filtersByDomain
        val domains = mutableMapOf<String, Int>()
        for (i in 0 until index.filtersByDomainLength) {
            val pair = index.filtersByDomain(i)!!
            domains[pair.domain!!] = pair.filtersLength
        }
        assertTrue(domains.keys.contains("scifinow.co.uk"))
        assertEquals(3, domains["dcw50.com"])

        // filterBySelector
        val selectors = mutableListOf<String>()
        for (i in 0 until index.filterBySelectorLength) {
            selectors.add(index.filterBySelector(i)!!.selector!!)
        }
        assertTrue(selectors.contains(".side-ad-blocks"))

        // elemhideKnownFilters
        val elemHideKnownFilters = mutableListOf<String>()
        for (i in  0 until index.elemhideKnownFiltersLength) {
            elemHideKnownFilters.add(index.elemhideKnownFilters(i)!!)
        }
        assertTrue(elemHideKnownFilters.contains("##.ads_post_start"))

        // elemhideExceptionKnownExceptions
        val elemhideExceptionKnownExceptions = mutableListOf<String>()
        for (i in  0 until index.elemhideExceptionKnownExceptionsLength) {
            elemhideExceptionKnownExceptions.add(index.elemhideExceptionKnownExceptions(i)!!)
        }
        assertTrue(elemhideExceptionKnownExceptions.contains("fastquake.com#@#.adsense_single"))
    }

    @Test
    fun testByKey() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val inputStream = context.resources.openRawResource(R.raw.easylist_and_exceptionrules)
        val index = Index.getRootAsIndex(ByteBuffer.wrap(inputStream.readBytes()))

        // filtersByDomain
        val filtersByDomainByKey = index.filtersByDomainByKey("dcw50.com")
        assertNotNull(filtersByDomainByKey)
        assertEquals(3, filtersByDomainByKey!!.filtersLength)

        // filterBySelector
        val filterBySelectorByKey = index.filterBySelectorByKey(".side-ad-blocks")
        assertNotNull(filterBySelectorByKey)
        assertEquals(
            "##.side-ad-blocks",
            filterBySelectorByKey!!.filter!!.activeFilter!!.filter!!.filterText)

        // filtersByKeyword
        val filtersByKeyword = index.filtersByKeywordByKey("")
        assertNotNull(filtersByKeyword)
        assertEquals(129, filtersByKeyword!!.filtersLength)

        // elemhideExceptionExceptions
        val exceptions = index.elemhideExceptionExceptionsByKey("#AD_Top")
        assertNotNull(exceptions)
        assertEquals(1, exceptions!!.filtersLength)
        assertEquals(
            "opensubtitles.org#@##AD_Top",
            exceptions.filters(0)!!.activeFilter!!.filter!!.filterText)
    }
}